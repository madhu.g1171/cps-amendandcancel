updateChargesForDeposits();
/*
 * Updates the card charge amounts for each of the deposit types.
 */
function updateChargesForDeposits()
{
	//var depositTypes = $$('input[name=depositType]');
	//var deposCardChargeDivs =  $$('#creditCardChargeAmt');
	//var depositAmount = $$('#depositAmt');
	var cardCharge = null;
	var amendmentCardCharge = null;
	depositAmount = depositAmountsMap.get("fullCost");
	if(PaymentInfo.amendmentChargeAmount != '')
	{
	   amendmentCardCharge = calculateCardCharge(PaymentInfo.amendmentChargeAmount);
	   amendmentCardCharge = parseFloat(PaymentInfo.amendmentChargeAmount) + parseFloat(amendmentCardCharge);
	   if(applyCreditCardSurcharge=='true'){
	   document.getElementById("amendmentChargeWithCC").innerHTML = parseFloat(amendmentCardCharge).toFixed(2);
	   document.getElementById("creditCardChargeAmt_fullCost").innerHTML = parseFloat(amendmentCardCharge).toFixed(2);
	   }
	   document.getElementById("debitCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
	   document.getElementById("thomsonCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
	   if(document.getElementById("giftCardChargeAmt_fullCost") != null)
	   {
	      document.getElementById("giftCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
	   }
	}
	else
	{
	   cardCharge = calculateCardCharge(depositAmount);
	   cardCharge = parseFloat(depositAmount) + parseFloat(cardCharge);
	   if(applyCreditCardSurcharge=='true'){
	   document.getElementById("creditCardChargeAmt_fullCost").innerHTML = parseFloat(cardCharge).toFixed(2);
	}
}
}

/**
 ** This method calculates the card charge for the deposit amount.
**/
function calculateCardCharge(amount)
{
  var applicableCharges= new Array();
  var cardCharge=0.0;

    applicableCharges = configurableCardCharge.split(",");


    if(applicableCharges!=null)
    {
     if(applicableCharges[0] > 0.0)
      {
      cardCharge = parseFloat(amount * (applicableCharges[0]/100));
    }
       if (parseFloat(applicableCharges[1]) > 0.0 && parseFloat(cardCharge) < parseFloat(applicableCharges[1]))
      {
        cardCharge = parseFloat(applicableCharges[1]);
      }
      if (parseFloat(applicableCharges[2]) > 0.0 && parseFloat(cardCharge) > parseFloat(applicableCharges[2]))
      {
         cardCharge = parseFloat(applicableCharges[2]);
      }
    }
    return parseFloat(Math.round(cardCharge * 100) / 100).toFixed(2);
}


/**
 ** Refreshes the PaymentInfo. A central function responsible for
 ** keeping data integrity of different amounts in
 ** in this payment for client side validation.
**/
function updatePaymentInfo(selectedDepositType)
{
PaymentInfo.depositType = trimSpaces((selectedDepositType));
   if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
   {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
   }
   else
   {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
   }
   PaymentInfo.selectedDepositAmount = PaymentInfo.amendmentChargeAmount;
   calCardCharge  = calculateCardCharges(PaymentInfo.selectedDepositAmount);

   PaymentInfo.totalCardCharge = calCardCharge;
   PaymentInfo.calculatedPayableAmount = roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2); //parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge).toFixed(2);
   PaymentInfo.calculatedTotalAmount = roundOff((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge , 2); //parseFloat((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge).toFixed(2);

   var totalCharge = parseFloat(PaymentInfo.selectedDepositAmount) + parseFloat(calCardCharge);
   document.getElementById("creditCardChargeFinalAmt").innerHTML = totalCharge.toFixed(2);
   document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   if(document.getElementById("giftCardFinalAmt") != null)
   {
      document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   }
   document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
}

/**
 ** This method sets the default deposit option and payment method on load of the payment page.
**/
function setDefaultDepositOption(){
	document.getElementById('issue').style.display = 'none';
	if(isNewHoliday == 'false'){
		document.CheckoutPaymentDetailsForm.reset();
	}
	displayCreditCharges('debitcardType');
	//updatePaymentInfo('fullCost');
	//	selectedpaymentMode('full-amt');

}