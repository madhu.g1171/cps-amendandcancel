<c:set var="cardChargeDetails" value="${bookingInfo.cardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<c:set var="cardCharge" value="${cardChargeArray[0]}" />
<c:set var="maxCardCharge" value="${cardChargeArray[2]}" />
<c:set var="maxCardCharge" value="${cardChargeArray[2]}" />
<c:set var="maxCap" value="${maxCardCharge}" />
<fmt:parseNumber var="parsemaxCap" type="number" value="${maxCap}" />
<c:set var="CONST_FULL_COST">fullCost</c:set>
<c:set var="CONST_LOW_DEPOSIT"></c:set>
<fmt:formatNumber var="fullAmount"
    value="${bookingComponent.payableAmount.amount-bookingInfo.calculatedDiscount.amount}"
    type="number" maxFractionDigits="2" minFractionDigits="2"
    pattern="#####.##" />

								   <div class="component pay-modes">
								   <input type="hidden" name="payment_0_type" id="payment_0_type">
										<div class="section-heading bf-first">
											<h2>Secure online payment</h2>
										</div>
										<c:if test="${not empty paySummaryAmendmentCharge}">
											<div class="b marg-bottom-20">
												<h2 class="marg-bottom-20">Payment due today</h2>
												<div class="bdr-blue marg-bottom-10">
													<div class="box bg-sand">
														<h2 class="title"> ${poundOrEuro}<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paySummaryAmendmentCharge}" pattern="#####.##"/></h2>
														if(applyCreditCardSurcharge=='true'){
														<p class="marg-top--5 grey-med">${poundOrEuro}<span id="amendmentChargeWithCC"><c:out value="${paySummaryAmendmentCharge}"/></span> if paying by credit card.</p>
														}
													</div>
													<p class="pad-10">
														<a class="size-15" href="<c:out value='${summaryPageURL}'/>">Edit changes</a>
													</p>
												</div>
											</div>
										</c:if>
										<c:if test="${(clientapp != 'ANCTHFO') && (fullAmount > 0.0)}">
										    <c:if test="${(clientapp != 'ANCFJFO') && (fullAmount > 0.0)}">
										   		 <%@include file="depositDetails.jspf"%>
											</c:if>
										</c:if>
										<input type="hidden" id="cardtype" name="cardtype" value="Debit">
										<h2 class="marg-bottom-30">How would you like to pay?</h2>
										<div class="b pay-box" id="debitcardType" onclick="displayCreditCharges(this.id)">
											<div class="box with-trigger bdr-blue marg-bottom-10 active">
												<h2 class="title">Debit card</h2>
												<p class="marg-top--5 marg-bottom-10 grey-med">No extra charge.</p>
												<p class="grey-med">There is no extra charge if paying by debit card. We accept all major debit cards.</p>
												<div class="select-block bg-sand b grey-dark full-hit">

													<!-- Note for js: the whole container is clickable! -->
													<span class="radio active" id="debit"></span><span class="rtext">Selected</span>
													<span class="hide" id="debitCardChargeAmt_fullCost"><c:out value="${fullAmount}" /></span>

												</div>
											</div>
										</div>

										<p class="marg-bottom-10 text-c font-i">Or...</p>
										<c:choose>
											<c:when test="${clientapp == 'ANCFALCON'}">
												<div class="b pay-box" id="creditcardType" onclick="displayCreditCharges(this.id)">
													<div class="box with-trigger bdr-blue marg-bottom-20">
														<h2 class="title">Credit card</h2>
														<p class="marg-top--5 marg-bottom-10 grey-med">+<fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% credit card surcharge.</p>
														<p class="grey-med">There is a <fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% surcharge <c:if test="${parsemaxCap > 0}">(which is capped at ${poundOrEuro}<c:out value="${maxCardCharge}"/>)</c:if> if paying by credit card. We accept all major credit cards.</p>
														<div class="select-block b grey-dark full-hit">
															<!-- Note for js: the whole container is clickable! -->
															<span class="radio" id="credit"></span><span class="rtext">Select</span>
															<span class="hide" id="creditCardChargeAmt_fullCost"></span>
														</div>
													</div>
												</div>
											</c:when>
											<c:when test="${clientapp == 'ANCFJFO'}">
												<div class="b pay-box" id="creditcardType" onclick="displayCreditCharges(this.id)">
													<div class="box with-trigger bdr-blue marg-bottom-20">
														<h2 class="title">Credit card</h2>
														<p class="marg-top--5 marg-bottom-10 grey-med">+<fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% credit card surcharge.</p>
														<p class="grey-med">There is a <fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% surcharge <c:if test="${parsemaxCap > 0}">(which is capped at ${poundOrEuro}<c:out value="${maxCardCharge}"/>)</c:if> if paying by credit card. We accept all major credit cards.</p>
														<div class="select-block b grey-dark full-hit">
															<!-- Note for js: the whole container is clickable! -->
															<span class="radio" id="credit"></span><span class="rtext">Select</span>
															<span class="hide" id="creditCardChargeAmt_fullCost"></span>
														</div>
													</div>
												</div>
											</c:when>
											<c:otherwise>
												<div class="b pay-box" id="creditcardType" onclick="displayCreditCharges(this.id)">
													<div class="box with-trigger bdr-blue marg-bottom-10">
														<h2 class="title">Credit card</h2>
														<c:choose>
															<c:when test="${clientapp != 'ANCFALCON'}">
																<p id="cardCharges" class="marg-top--5 marg-bottom-10 grey-med">+<fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% credit card surcharge.</p>
																<p id="credit-text" class="grey-med">There is a <fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% surcharge <c:if test="${parsemaxCap > 0}">
																(which is capped at ${poundOrEuro}<c:out value="${maxCardCharge}"/>)</c:if> if paying by credit card. We accept all major credit cards.</p>
																<p style="display: none;" id="thomsoncard" class="marg-top--5 marg-bottom-10 grey-med">+<fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% credit card surcharge.</p>
																<p style="display: none;" id="thomsoncredit-text" class="grey-med">There is a <fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% surcharge <c:if test="${parsemaxCap > 0}">(which is capped at ${poundOrEuro}<c:out value="${maxCardCharge}"/>)</c:if>  if paying by credit card. We accept all major credit cards.</p>
															</c:when>
															<c:otherwise>
																<p class="marg-top--5 marg-bottom-10 grey-med">+<fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% credit card surcharge.</p>
																<p class="grey-med">There is a <fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% surcharge <c:if test="${parsemaxCap > 0}">(which is capped at ${poundOrEuro}<c:out value="${maxCardCharge}"/>)</c:if> if paying by credit card. We accept all major credit cards.</p>
															</c:otherwise>
														</c:choose>
														<div class="select-block b grey-dark full-hit">
															<!-- Note for js: the whole container is clickable! -->
															<span class="radio" id="credit"></span><span class="rtext">Select</span>
															<span class="hide" id="creditCardChargeAmt_fullCost"></span>
															<c:if test="${(clientapp == 'ANCTHFO') || (clientapp == 'ANCFJFO') || ((clientapp != 'ANCFALCON') && (fullAmount <= 0.0))}">
															   <span class="hide" id="thomsonCardChargeAmt_fullCost"><c:out value="${fullAmount}" /></span>
															</c:if>
														</div>
													</div>
												</div>

												<p class="marg-bottom-10 text-c font-i">Or...</p>

												<div class="b pay-box" id="giftcardType" onclick="displayCreditCharges(this.id)">
													<div class="box with-trigger bdr-blue marg-bottom-20">
														<h2 class="title">Gift card</h2>
														<p class="grey-med">If you've got a Thomson or First Choice gift card then you can pay using the balance on your card. You can <a href="https://portal.prepaytec.com/chopinweb/balanceCheck.do?customerCode=202192018122&amp;loc=en&amp;showPin=0&amp;brandingCode=bal_tui" target="_blank"> find out the balance</a> online or by calling 0203 130 0133.</p>
														<div class="select-block b grey-dark full-hit">
															<!-- Note for js: the whole container is clickable! -->
															<span class="radio" id="gift"></span><span class="rtext">Select</span>
															<span class="hide" id="giftCardChargeAmt_fullCost"><c:out value="${fullAmount}" /></span>
														</div>
													</div>
												</div>
											</c:otherwise>
										</c:choose>
										<div class="disNone" id="creditCardCharges" onclick="displayCreditCharges(this.id)">
											<c:choose>
												<c:when test="${(clientapp == 'ANCTHFO') || (fullAmount <= 0.0)}">
												    <p class="size-18">You're about to pay <span class="creditCardChargeFinalAmt">${poundOrEuro}</span><span class="creditCardChargeFinalAmt" id="creditCardFinalAmt"><c:if test="${not empty paySummaryAmendmentCharge}"><c:out value="${paySummaryAmendmentCharge}"/></c:if></span> by Credit Card</p>
												</c:when>
												<c:when test="${(clientapp == 'ANCFJFO') || (fullAmount <= 0.0)}">
												    <p class="size-18">You're about to pay <span class="creditCardChargeFinalAmt">${poundOrEuro}</span><span class="creditCardChargeFinalAmt" id="creditCardFinalAmt"><c:if test="${not empty paySummaryAmendmentCharge}"><c:out value="${paySummaryAmendmentCharge}"/></c:if></span> by Credit Card</p>
												</c:when>
												<c:otherwise>
											<p class="size-18">You're about to pay <span class="creditCardChargeFinalAmt">${poundOrEuro}</span><span class="creditCardChargeFinalAmt" id="creditCardChargeFinalAmt"><c:if test="${not empty paySummaryAmendmentCharge}"><c:out value="${paySummaryAmendmentCharge}"/></c:if></span> by Credit Card</p>
												</c:otherwise>
											</c:choose>
											<p class="grey-med">Includes <fmt:formatNumber value="${cardCharge}" type="number" maxFractionDigits="2" minFractionDigits="0" pattern="#####.##" />% Credit Card Surcharge</p>
	                 					</div>
										<div class="disNone" id="debitCardCharges">
											<p class="size-18">You're about to pay  <span class="debitCardChargeFinalAmt">${poundOrEuro}</span><span class="creditCardChargeFinalAmt" id="debitCardFinalAmt"><c:if test="${not empty paySummaryAmendmentCharge}"><c:out value="${paySummaryAmendmentCharge}"/></c:if></span> by Debit Card</p>
										</div>
										<c:if test="${(clientapp != 'ANCFALCON') || (clientapp != 'ANCFJFO')}">
											<div class="disNone" id="thomsonCardCharges">
											    <p class="size-18">You're about to pay  <span class="creditCardChargeFinalAmt">${poundOrEuro}</span><span class="creditCardChargeFinalAmt" id="thomsonCardFinalAmt"><c:if test="${not empty paySummaryAmendmentCharge}"><c:out value="${paySummaryAmendmentCharge}"/></c:if></span> by TUI Credit Card</p>
											</div>
											<div class="disNone" id="giftCardCharges" onclick="displayCreditCharges(this.id)">
												<p class="size-18">You're about to pay  <span class="giftCardFinalAmt">${poundOrEuro}</span><span class="creditCardChargeFinalAmt" id="giftCardFinalAmt"><c:if test="${not empty paySummaryAmendmentCharge}"><c:out value="${paySummaryAmendmentCharge}"/></c:if></span> by Gift Card</p>
											</div>
										</c:if>
									</div>


<input type="hidden" name="total_transamt"  id="total_transamt"  value="NA" />
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>
<c:choose>
	<c:when test="${(bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON') && (fullAmount > 0.0)}">
		<script type="text/javascript" src="/cms-cps/amendandcancel/ancfalcon/js/functions_depositSection.js"></script>
	</c:when>
	<c:when test="${(bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON') && (fullAmount <= 0.0)}">
		<script type="text/javascript" src="/cms-cps/amendandcancel/ancfalcon/js/functions_depositSection_fullyPaid.js"></script>
	</c:when>
	<c:when test="${(bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHFO') || (bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO') || (fullAmount <= 0.0)}">
		<script type="text/javascript" src="/cms-cps/amendandcancel/ancthfo/js/functions_depositSection.js"></script>
		<c:if test="${(bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO')}" >
		<script type="text/javascript" src="/cms-cps/amendandcancel/ancfjfo/js/functions_depositSection.js"></script>
		<script type="text/javascript" src="/cms-cps/amendandcancel/ancfjfo/js/functions_depositSection_fullyPaid.js"></script>
	     <script type="text/javascript" src="/cms-cps/amendandcancel/ancfjfo/js/paymentUtils_fullyPaid.js"></script>
		</c:if>
	</c:when>
	<c:otherwise>
		<script type="text/javascript" src="/cms-cps/amendandcancel/js/functions_depositSection.js"></script>
	</c:otherwise>
</c:choose>

